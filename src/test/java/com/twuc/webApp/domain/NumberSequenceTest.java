package com.twuc.webApp.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberSequenceTest {

    @Test
    void should_generate_number_sequence() {
        NumberSequence numberSequence = new NumberSequence();
        assertEquals(4, numberSequence.size());
    }

    @Test
    void testToString() {
        NumberSequence numberSequence = new NumberSequence("1234");
        assertEquals("1234", numberSequence.toString());
    }

    @Test
    void should_get_game_hint() {
        NumberSequence firstSequence = new NumberSequence("1235");
        NumberSequence secondSequence = new NumberSequence("1354");
        NumberSequence thirdSequence = new NumberSequence("1235");

        GameHint firstHint = new GameHint("1A2B", false);
        GameHint secondHint = new GameHint("4A0B", true);

        assertEquals(firstHint, firstSequence.getHint(secondSequence));
        assertEquals(secondHint, firstSequence.getHint(thirdSequence));
    }
}
