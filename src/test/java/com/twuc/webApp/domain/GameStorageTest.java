package com.twuc.webApp.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GameStorageTest {

    @Test
    void should_init_game_storage() {
        GameStorage storage = new GameStorage();

        NumberGame firstGame = new NumberGame();
        NumberGame secondGame = new NumberGame();

        storage.addGame(firstGame);
        storage.addGame(secondGame);

        assertEquals(2, storage.getGameCount());
        assertEquals(firstGame, storage.getGameById(1));
    }
}
