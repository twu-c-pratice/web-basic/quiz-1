package com.twuc.webApp.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberGameTest {
    @Test
    void should_init_a_number_game() {
        NumberGame game = new NumberGame();
        assertEquals(4, game.getNumSequence().size());
    }
}
