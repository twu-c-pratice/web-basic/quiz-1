package com.twuc.webApp.domain;

public class GameStatus {
    private int id;
    private String answer;

    public GameStatus() {}

    public GameStatus(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
